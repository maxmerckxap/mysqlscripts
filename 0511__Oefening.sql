USE ModernWays;
INSERT INTO huisdieren (
Baasje,
Naam,
Leeftijd,
Soort
)
VALUES 
('vincent', 'misty', '6', 'hond'),
('christiane', 'ming', '8', 'hond'),
('esther', 'bientje', '6', 'kat'),
('jommeke', 'flip', '75', 'papegaai'),
('villads', 'berto', '1', 'papegaai'),
('bert', 'ming', '7', 'kat'),
('thais', 'suerta', '2', 'hond'),
('lyssa', 'Фёдор', '1', 'hond');