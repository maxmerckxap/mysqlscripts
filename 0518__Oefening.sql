USE ModernWays;
ALTER TABLE huisdieren ADD COLUMN Geluid VARCHAR(20) CHAR SET utf8mb4;
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET geluid = 'woef' WHERE soort = 'hond';
UPDATE huisdieren SET geluid = 'miauw' WHERE soort = 'kat';
SET SQL_SAFE_UPDATES = 1;